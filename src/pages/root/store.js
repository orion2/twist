
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import axios from 'axios'

// import router from '../../router'
const yaml = require('js-yaml')
// const fs = require('fs')

// state.grid = yaml.safeLoad(state.yaml).grid

export default new Vuex.Store({
  state: {
    csscolor: {
      fk: null,
      fo: null,
    },
    pages: { },
    active: {
      now: '',
      elm: null
    },
    grid: {},
    yaml: `
      flexcenter: &flctr
        display: flex
        align-items: center
        justify-content: center
      
      grid:
        mobile: &mobile
          display: grid
          grid-template-columns: repeat(6, 16.6666666667vw)
          grid-template-rows: repeat(20,5vh)
        quatrek: &4k
          display: grid
          grid-template-columns: repeat(6, 7.5vw)
          grid-template-rows: repeat(20,5vh)
          margin-left: 27.5vw   
        style: *mobile
      
        topbar:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 1
            grid-template-columns: repeat(1,100%)
            grid-template-rows: repeat(1,100%)
          content:
            - TW!$T
          content_style: 
            <<: *flctr
            background-color: #F2F2F2
      
        topnav:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 2
            grid-template-columns: repeat(6,16.6666666667%)
            grid-template-rows: repeat(1,100%)
          content:
          content_style:
            <<: *flctr
            font-size: 75%
            background-color: '#25D9B8'
            color: black
      
        city:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 3 / span 8
            grid-template-columns: repeat(4,25%)
            grid-template-rows: repeat(4,25%)
          content:
          content_style:
            <<: *flctr
            font-size: 75%
      
        timetablehours:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 11
            grid-template-columns: repeat(4,25%)
            grid-template-rows: repeat(1,100%)
          content:
            - a1h <artists-id[1]-hours>
            - a2h <id[2]>
            - a3h <id[3]>
            - a4h <id[4]>
          content_style:
            <<: *flctr
            font-size: 75%
            background-color: '#011F26'
            color: #F2F2F2
      
        timetableartists:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 12 / span 4
            grid-template-columns: repeat(1,100%)
            grid-template-rows: repeat(4,25%)
          content:
            - a1n <artist-id[1]-name>
            - a2n <id[2]>
            - a3n <id[3]>
            - a4n <id[4]>
          content_style:
            <<: *flctr
            font-size: 75%
      
        myguest:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 16
            grid-template-columns: repeat(3,33.3333333333%)
            grid-template-rows: repeat(1, 100%)
          content:
            - <guestlist-waiting>
            - <guestlist-accepted>
            - <guestlist-refused>
          content_style:
            <<: *flctr
            font-size: 75%
      
        footerplayer:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 20
            grid-template-columns: repeat(3,33.3333333333%)
            grid-template-rows: repeat(1, 100%)
          content:
            - play-artist-link
            - player-volume
            - 'wallet'
          content_style:
            <<: *flctr
            font-size: 75%
      
        personsearcher:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 17
            grid-template-columns: repeat(10,10%)
            grid-template-rows: repeat(1, 100%)
          content:
            - who
            - name
          content_style:
            <<: *flctr
            font-size: 75%
      
        persons:
          style:
            display: grid
            grid-column: 1 / span 6
            grid-row: 18 / span 2
            grid-template-columns: repeat(10,10%)
            grid-template-rows: repeat(1, 100%)
          content:
            - who
            - name
          content_style:
            <<: *flctr
            font-size: 75%    
    `
  },
  mutations: {
    __init__(state){
      // axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8'
      // axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'
      // axios.get('http://api.aendlyx.tech//')
      // .then(rsp=>{
      //   state.grid=rsp.data
      //   console.log(rsp)
      // })
      state.grid = yaml.safeLoad(state.yaml).grid
      console.log(state.grid)
    },
    over(state, event){
      let id = event.target.id
      let domitem = document.getElementById(id)
      state.csscolor.bk=domitem.style.backgroundColor
      state.csscolor.fo=domitem.style.color
      domitem.style.backgroundColor = '#CCCFD1'
      domitem.style.color = 'black'
    },
    leave(state, event){
      let id = event.target.id
      let domitem = document.getElementById(id)
      domitem.style.backgroundColor = state.csscolor.bk
      domitem.style.color = state.csscolor.fo
    },
    clicked(state, event){
      state.active.now = event.target.id
    },
    mobile(state){
      this.$store.state.grid.grid.style = this.$store.state.grid.grid['mobile']
    },
    quatrek(state){
      state.grid.grid.style = state.grid.grid['4k']
    },
    gettopnav(state){
      axios.get('https://api.aendlyx.tech/projets/2')
      .then(rsp=>{
        const yaml = require('js-yaml')
        let yml = yaml.safeLoad(rsp.data.yaml)
        state.grid.topnav.content=yml.et
      })
      axios.get('https://api.aendlyx.tech/projets/3')
      .then(rsp=>{
        const yaml = require('js-yaml')
        let yml = yaml.safeLoad(rsp.data.yaml)
        state.grid.city.content=yml.city
      })
    },
  },
  getters: {
  }
})
